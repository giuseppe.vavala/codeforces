// https://codeforces.com/problemset/problem/1631/A

#include <iostream>
#include <stdlib.h>
#include <algorithm>
using namespace std;

void read(int *a, int *b, int n)
{
    for (int i = 0; i < n; i++)
        scanf("%d", &a[i]);

    for (int i = 0; i < n; i++)
        scanf("%d", &b[i]);
}

int find_max(int *a, int n)
{
    int ret = 0;
    for (int i = 0; i < n; i++)
    {
        if (a[i] > a[ret])
            ret = i;
    }
    return (ret);
}

void swap(int *a, int *b, int i)
{
    int tmp = a[i];
    a[i] = b[i];
    b[i] = tmp;
}

int main()
{
    int num_test, n, index_max_a, index_max_b, sol, sol1, sol2;
    int new_max_a_sol1, new_max_b_sol1, new_max_a_sol2, new_max_b_sol2;
    bool finito;

    cin >> num_test;

    for (int test = 0; test < num_test; test++)
    {
        cin >> n;

        int a[n];
        int b[n];
        read(a, b, n);

        index_max_a = find_max(a, n);
        index_max_b = find_max(b, n);

        sol = a[index_max_a] * b[index_max_b];
        finito = false;
        while (!finito)
        {
            swap(a, b, index_max_a);
            new_max_a_sol1 = find_max(a, n);
            new_max_b_sol1 = find_max(b, n);
            sol1 = a[new_max_a_sol1] * b[new_max_b_sol1];
            swap(a, b, index_max_a);

            swap(a, b, index_max_b);
            new_max_a_sol2 = find_max(a, n);
            new_max_b_sol2 = find_max(b, n);
            sol2 = a[new_max_a_sol2] * b[new_max_b_sol2];

            if ((sol1 >= sol) && (sol2 >= sol))
            {
                finito = true;
            }
            else if (sol2 <= sol1)
            {
                sol = sol2;
                index_max_a = new_max_a_sol2;
                index_max_b = new_max_b_sol2;
            }
            else
            {
                sol = sol1;
                swap(a, b, index_max_b);
                swap(a, b, index_max_a);
                index_max_a = new_max_a_sol1;
                index_max_b = new_max_b_sol1;
            }
        }

        printf("%d\n", sol);
    };
}