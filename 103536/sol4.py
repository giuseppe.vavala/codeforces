import sys
from time import time


tempo1 = 0
tempo2 = 0
tempo3 = 0
tempo4 = 0

sys.setrecursionlimit(3000)

N, G = map(int, input().split())
S = []

for i in range (0, N):
    S.append(int(input()))


Gkcomb = [0 for x in range(0, G)]
cache = [{} for x in range(0, G)]

def trovaMin(partial_sum, k_sum, valMax, index):   
    global tempo1
    global tempo2
    global tempo3
    global tempo4


    # Guardo se posso usare la cache
    start = time()
    if valMax in cache[index]:
        return cache[index][valMax] + partial_sum
    tempo1 += time() - start 

    if (len(Gkcomb) - 1 == index):
        start = time()
        Gkcomb[index] = valMax
        ret = 0
        for Si in S[k_sum: k_sum + valMax]:
            ret += valMax * Si
        tempo2 += time() - start 
        return partial_sum + ret
    else:
        start = time()
        kmin = 1
        Gkcomb[index] = 1
        
        #Calcolo Somma parziare
        new_partial_sum = partial_sum + S[k_sum]
        # Trovo min
        tempo3 += time() - start 
        min = trovaMin (new_partial_sum, k_sum + 1, valMax, index + 1)

        for k in range(2, valMax+1):
            start = time()
            #Calcolo Somma parziare
            Gkcomb[index] = k
            for Si in S[k_sum: k_sum + k -1]:
                new_partial_sum += Si
            new_partial_sum += k * S[k_sum + k -1]

            tempo3 += time() - start 
            # Trovo min
            tmp = trovaMin (new_partial_sum, k_sum + k, valMax - k + 1, index + 1)
            if (tmp < min): 
                min = tmp
                kmin = k        

        start = time()
        Gkcomb[index] = kmin        
        cache[index][valMax] = min - partial_sum
        tempo4 += time() - start 
        return min 
    

print (trovaMin (0, 0, N - G + 1, 0))
print (tempo1, tempo2, tempo3, tempo4)
