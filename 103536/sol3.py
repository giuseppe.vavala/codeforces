N, G = map(int, input().split())
S = []

for i in range (0, N):
    S.append(int(input()))


Gkcomb = [0 for x in range(0, G)]
cache = [{} for x in range(0, G)]

def trovaMin(partial_sum, k_sum, valMax, index):    
    # Guardo se posso usare la cache
    if valMax in cache[index]:
        return cache[index][valMax] + partial_sum

    if (len(Gkcomb) - 1 == index):
        Gkcomb[index] = valMax
        sum = partial_sum
        for Si in S[k_sum: k_sum + valMax]:
            sum += valMax * Si
        return sum
    else:

        #Calcolo Somma parziare
        kmin = 1
        Gkcomb[index] = 1
        new_partial_sum = partial_sum
        for Si in S[k_sum: k_sum + 1]:
            new_partial_sum += Si
        # Trovo min
        min = trovaMin (new_partial_sum, k_sum + 1, valMax, index + 1)
        
        for k in range(2, valMax+1):
            #Calcolo Somma parziare
            Gkcomb[index] = k
            new_partial_sum = partial_sum
            for Si in S[k_sum: k_sum + k]:
                new_partial_sum += k * Si
            # Trovo min
            tmp = trovaMin (new_partial_sum, k_sum + k, valMax - k + 1, index + 1)
            if (tmp < min): 
                min = tmp
                kmin = k        
        Gkcomb[index] = kmin        
        cache[index][valMax] = min - partial_sum
        return min 

print (trovaMin (0, 0, N - G + 1, 0))
