N, G = map(int, input().split())
S = []

for i in range (0, N):
    S.append(int(input()))


def valuta(Gk):
    tot = 0
    i = 0
    for k in Gk:
        parz = 0
        for el in S[i:i+k]:
            i += 1
            parz += el
        tot += k * parz
    
    return tot

Gkcomb = ['?' for x in range(0, G)]


def trovaMin(valMax, index):
    if (len(Gkcomb) - 1 == index):
        Gkcomb[index] = valMax
        return valuta(Gkcomb)
    else:
        min = None
        kmin = None
        for k in range(1, valMax+1):
            Gkcomb[index] = k
            tmp = trovaMin (valMax - k + 1, index + 1)
            if (not min) or (tmp < min): 
                min = tmp
                kmin = k
        
        Gkcomb[index] = kmin
        return min 

print (trovaMin (N - G + 1, 0))
