N, G = map(int, input().split())
S = []

for i in range (0, N):
    S.append(int(input()))
 
 
def is_finish(comb):
    for el in comb:
        if (el < G-1): return False
    return True
 
def inc(comb, index):
    if comb[index] < (G-1):
        comb[index] += 1
    else:
        comb[index] = 0
        inc(comb, index -1)
 
def valuta(comb):
    Gk = []
    
    for Gnum in range(0,G):
        Gk.append( comb.count(Gnum) )
 
    tot = 0
    for i in range (0, N):
        tot += Gk[comb[i]] * S[i]
    
    return tot
 
combinations = [0 for x in range(0, N)]
 
min = valuta(combinations)
min_comb = combinations
while not is_finish(combinations):  
    inc(combinations, N-1)
    val = valuta(combinations)
    if val < min:
        min = val
        min_comb = combinations
 
print (min)