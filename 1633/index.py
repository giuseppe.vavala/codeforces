# https://codeforces.com/contest/1633/problem/A



multiple = [((3 - len(str(x))) * 'x' + str(x)) for x in range(7,999,7)]

num = int(input())
vals = []
for i in range (0, num):
    val = input()
    vals.append((3 - len(val)) * 'x' + val)

def cambi(val1, val2):
    diff = 0
    for i in range(0, len(val1)):
        if (val1[i] != val2[i]):
            if (val1[i] == 'x') or (val2[i] == 'x'):
                diff += 5
            else:
                diff += 1
    return diff

def trova_min(val):
    min = cambi(val, multiple[0])
    if (min == 1): return multiple[0]
    val_min = multiple[0]
    for test in multiple[1:]:
        tmp = cambi(val, test)
        if (tmp == 1):
            return test
        elif tmp < min:
            min = tmp
            val_min = test
    return val_min

for val in vals:
    if (val in multiple):
        print (val.replace('x',''))
    else:
        print (trova_min(val).replace('x',''))
        

